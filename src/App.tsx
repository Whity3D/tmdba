import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { makeStore } from './store/config';
import RootNavigationContainer from "./navigation/RootNavigationContainer";

const { store, persistor } = makeStore();

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
        <RootNavigationContainer />
      </PersistGate>
    </Provider>
  );
};

export default App;
