import React, { FC } from 'react';
import { StyleSheet, Text } from 'react-native';

const styles = StyleSheet.create({
  appTitle: {
    padding: 10,
    fontSize: 32,
    fontWeight: 'bold',
    color: '#fff',
    borderBottomColor: '#fff',
    borderBottomWidth: 2,
    backgroundColor: '#0f1d22',
  },
});

const AppTitle: FC = () => {
  return <Text style={styles.appTitle}>TMDBA</Text>;
};
export default AppTitle;
