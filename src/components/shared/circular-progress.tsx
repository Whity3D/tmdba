import React, { FC, useMemo } from 'react';
import { StyleSheet, Text, useColorScheme } from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

interface ICircularProgressProps {
  value: number;
  size?: number;
  barWidth?: number;
}

const calculateStyles = (isDark: boolean) =>
  StyleSheet.create({
    value: {
      fontSize: 14,
      fontWeight: 'bold',
      color: isDark ? '#ffffff' : '#000000',
    },
  });

const CircularProgress: FC<ICircularProgressProps> = ({
  value,
  size = 44,
  barWidth = 5,
}) => {
  const isDarkMode = useColorScheme() === 'dark';
  const styles = useMemo(() => calculateStyles(isDarkMode), [isDarkMode]);
  return (
    <AnimatedCircularProgress
      size={size}
      width={barWidth}
      fill={value}
      backgroundColor={'#423d0f'}
      tintColor={'#d2d531'}
      rotation={0}
      lineCap={'round'}
    >
      {() => <Text style={styles.value}>{value}%</Text>}
    </AnimatedCircularProgress>
  );
};

export default CircularProgress;
