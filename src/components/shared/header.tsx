import React, { FC } from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 300,
    backgroundColor: '#0f1d22',
  },
  mainTitle: {
    fontSize: 48,
    fontWeight: '700',
  },
  title: {
    fontSize: 32,
    fontWeight: '600',
  },
});

const Header: FC = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.mainTitle}>Добро пожаловать.</Text>
      <Text style={styles.title}>
        Миллионы фильмов, сериалов и людей. Исследуйте сейчас.
      </Text>
    </View>
  );
};

export default Header;
