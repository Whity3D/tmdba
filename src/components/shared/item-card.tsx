import React, { FC, useCallback } from 'react';
import {
  GestureResponderEvent,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { useAppSelector } from '../../store/hooks';
import { selectSecureBaseUrl } from '../../store/configuration';

interface ItemCardProps {
  id: number;
  posterPath: string | null;
  title: string;
  rating: number;
  onPress?: (id: number, title: string) => void;
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'flex-start',
    padding: 5,
    alignItems: 'center',
    width: 120,
  },
  poster: {
    width: 100,
    height: 150,
    borderRadius: 8,
    resizeMode: 'contain',
  },
  title: {
    textAlign: 'center',
  },
});

const ItemCard: FC<ItemCardProps> = ({
  id,
  posterPath,
  title,
  rating,
  onPress,
}) => {
  const secureUrl = useAppSelector(selectSecureBaseUrl);

  const handlePress = useCallback(
    (_: GestureResponderEvent) => {
      if (onPress) {
        onPress(id, title);
      }
    },
    [onPress, id, title],
  );

  return (
    <TouchableOpacity onPress={handlePress}>
      <View style={styles.container}>
        {posterPath && secureUrl ? (
          <Image
            style={styles.poster}
            source={{ uri: `${secureUrl}/original/${posterPath}` }}
          />
        ) : null}
        <Text>{rating.toFixed(1)}</Text>
        <Text textBreakStrategy="simple" style={styles.title}>
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default ItemCard;
