const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';

export default {
  light: {
    text: '#000',
    background: '#fff',
    tint: tintColorLight,
    tabIconDefault: '#f552b0',
    tabIconSelected: '#ffffff',
    touchableOpacity: 'transparent',

    primary: '#00cbf1',
    card: 'rgb(255, 255, 255)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  },
  dark: {
    text: '#fff',
    background: '#0f1d22',
    tint: tintColorDark,
    tabIconDefault: '#f552b0',
    tabIconSelected: '#ffffff',
    touchableOpacity: 'transparent',

    primary: '#00cbf1',
    card: 'rgb(18, 18, 18)',
    border: 'rgb(39, 39, 41)',
    notification: 'rgb(255, 69, 58)',
  },
};
