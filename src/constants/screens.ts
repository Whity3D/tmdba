export enum MainScreens {
  Main = 'Main',
  SignIn = 'SignIn',
  SignUp = 'SignUp',
  MovieDetails = 'MovieDetails',
  TVShowDetails = 'TVShowDetails',
}

export enum TabsScreens {
  Home = 'Home',
  Movies = 'Movies',
  TV = 'TV',
  Favorites = 'Favorites',
  Profile = 'Profile',
}
