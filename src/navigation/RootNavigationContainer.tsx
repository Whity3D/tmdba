import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

import { MainScreens } from '../constants/screens';
import MainScreen from '../screens/main';
import useColorScheme from '../hooks/useColorScheme';
import colors from '../constants/colors';
import SignInScreen from '../screens/sign-in';
import MovieDetails from '../screens/movie-details';
import TVShowDetails from '../screens/tv-show-details';

const Stack = createNativeStackNavigator();

const RootNavigationContainer = () => {
  const isDark = useColorScheme() === 'dark';

  return (
    <SafeAreaProvider>
      <NavigationContainer
        theme={{
          dark: isDark,
          colors: { ...colors[isDark ? 'dark' : 'light'] },
        }}
      >
        <Stack.Navigator>
          <Stack.Screen
            name={MainScreens.Main}
            component={MainScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name={MainScreens.SignIn}
            component={SignInScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name={MainScreens.MovieDetails}
            component={MovieDetails}
            options={{ title: 'Информация о фильме' }}
          />
          <Stack.Screen
            name={MainScreens.TVShowDetails}
            component={TVShowDetails}
            options={{ title: 'Информация о сериале' }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default RootNavigationContainer;
