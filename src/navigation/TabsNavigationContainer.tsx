import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { TabsScreens } from '../constants/screens';
import HomeScreen from '../screens/home';
import colors from '../constants/colors';
import { StyleSheet } from 'react-native';
import FavoritesScreen from '../screens/favorites';
import ProfileScreen from '../screens/profile';
import MoviesScreen from '../screens/movies';
import TVShowsScreen from '../screens/tv-shows';

const Tab = createBottomTabNavigator();

const styles = StyleSheet.create({
  tabBar: {
    position: 'absolute',
    backgroundColor: '#0f1d22',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    borderBottomLeftRadius: 40,
    borderBottomRightRadius: 40,
    height: 80,
  },
});

const TabsNavigationContainer = () => {
  return (
    <Tab.Navigator
      screenOptions={() => ({
        tabBarActiveTintColor: colors.dark.tabIconSelected,
        tabBarInactiveTintColor: colors.dark.tabIconDefault,
        tabBarShowLabel: false,
        tabBarStyle: styles.tabBar,
      })}
    >
      <Tab.Screen
        name={TabsScreens.Home}
        component={HomeScreen}
        options={{
          tabBarIcon: ({ size, color }) => (
            <Ionicons name="home-outline" size={size} color={color} />
          ),
          headerShown: false,
        }}
      />
      <Tab.Screen
        name={TabsScreens.Movies}
        component={MoviesScreen}
        options={{
          headerShown: false,
          tabBarIcon: ({ size, color }) => (
            <Ionicons name="videocam-outline" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name={TabsScreens.TV}
        component={TVShowsScreen}
        options={{
          headerShown: false,
          tabBarIcon: ({ size, color }) => (
            <Ionicons name="tv-outline" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name={TabsScreens.Favorites}
        component={FavoritesScreen}
        options={{
          headerShown: false,
          tabBarIcon: ({ size, color }) => (
            <Ionicons name="bookmark-outline" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name={TabsScreens.Profile}
        component={ProfileScreen}
        options={{
          headerShown: false,
          tabBarIcon: ({ size, color }) => (
            <Ionicons name="person-outline" size={size} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabsNavigationContainer;
