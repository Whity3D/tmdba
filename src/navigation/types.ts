import { ParamListBase } from '@react-navigation/routers/src/types';

import { MainScreens, TabsScreens } from '../constants/screens';

export interface ITabsNavigationScreenProps extends ParamListBase {
  [TabsScreens.Home]: undefined;
  [TabsScreens.Movies]: undefined;
  [TabsScreens.TV]: undefined;
  [TabsScreens.Favorites]: undefined;
  [TabsScreens.Profile]: undefined;
}

export interface IMainNavigationScreenProps extends ParamListBase {
  [MainScreens.Main]: undefined;
  [MainScreens.SignIn]: undefined;
  [MainScreens.SignUp]: undefined;
  [MainScreens.MovieDetails]: { movieId: number };
  [MainScreens.TVShowDetails]: { showId: number };
}
