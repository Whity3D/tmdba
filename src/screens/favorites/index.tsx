import React, { FC, useCallback } from 'react';
import {
  Button,
  FlatList,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { Colors } from 'react-native/Libraries/NewAppScreen';

import AppTitle from '../../components/shared/app-title';
import colors from '../../constants/colors';
import ItemCard from '../../components/shared/item-card';
import { IMovie } from '../../store/types';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ITabsNavigationScreenProps } from '../../navigation/types';
import { MainScreens, TabsScreens } from '../../constants/screens';
import { useAppSelector } from '../../store/hooks';
import { selectIsSigned } from '../../store/auth';

const styles = StyleSheet.create({
  container: {
    padding: 5,
  },
  listHeader: {
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
});

const FavoritesScreen: FC<
  NativeStackScreenProps<ITabsNavigationScreenProps, TabsScreens.Favorites>
> = ({ navigation }) => {
  const isDarkMode = useColorScheme() === 'dark';
  const tabBarHeight = useBottomTabBarHeight();

  const isSigned = useAppSelector(selectIsSigned);

  const favorites: IMovie[] = [];

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const handleButton = useCallback(() => {
    navigation.navigate(MainScreens.SignIn);
  }, []);

  return (
    <SafeAreaView>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        stickyHeaderIndices={[0]}
        style={backgroundStyle}
      >
        <AppTitle />
        <View
          style={{
            marginBottom: tabBarHeight,
            backgroundColor: isDarkMode
              ? colors.dark.background
              : colors.light.background,
          }}
        >
          <Text style={styles.listHeader}>Избранное</Text>
          {isSigned ? (
            favorites.length ? (
              <FlatList
                data={favorites}
                renderItem={({
                  item: { title, poster_path, id, vote_average },
                }) => (
                  <ItemCard
                    id={id}
                    key={id}
                    title={title}
                    posterPath={poster_path}
                    rating={vote_average}
                  />
                )}
              />
            ) : (
              <Text>В избранном ничего нет</Text>
            )
          ) : (
            <>
              <Text>Вы не авторизованы.</Text>
              <Text>Авторизуйтесь, чтобы увидеть избранное</Text>
              <Button title={'Войти'} onPress={handleButton} />
            </>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default FavoritesScreen;
