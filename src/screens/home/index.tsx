import React, { FC, useCallback, useEffect } from 'react';
import {
  FlatList,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { Colors } from 'react-native/Libraries/NewAppScreen';

import { useAppDispatch, useAppSelector } from '../../store/hooks';
import {
  getTrending as getTrendingMovies,
  selectTrendingMovies,
} from '../../store/movies';
import {
  getTrending as getTrendingTVShows,
  selectTrendingTVShows,
} from '../../store/tv-shows';
import Header from '../../components/shared/header';
import colors from '../../constants/colors';
import ItemCard from '../../components/shared/item-card';
import AppTitle from '../../components/shared/app-title';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ITabsNavigationScreenProps } from '../../navigation/types';
import { MainScreens, TabsScreens } from '../../constants/screens';

const styles = StyleSheet.create({
  container: {
    padding: 5,
  },
  listHeader: {
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
});

const HomeScreen: FC<
  NativeStackScreenProps<ITabsNavigationScreenProps, TabsScreens.Home>
> = ({ navigation }) => {
  const isDarkMode = useColorScheme() === 'dark';
  const tabBarHeight = useBottomTabBarHeight();

  const dispatch = useAppDispatch();
  const trendingMovies = useAppSelector(selectTrendingMovies);
  const trendingTvShows = useAppSelector(selectTrendingTVShows);

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const handlePressMovie = useCallback((id: number) => {
    navigation.navigate(MainScreens.MovieDetails, { movieId: id });
  }, []);

  const handlePressTVShow = useCallback((id: number) => {
    navigation.navigate(MainScreens.TVShowDetails, { showId: id });
  }, []);

  useEffect(() => {
    dispatch(getTrendingMovies('week'));
    dispatch(getTrendingTVShows('week'));
  }, []);

  return (
    <SafeAreaView>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        stickyHeaderIndices={[0]}
        style={backgroundStyle}
      >
        <AppTitle />
        <Header />
        <View
          style={{
            marginBottom: tabBarHeight,
            backgroundColor: isDarkMode
              ? colors.dark.background
              : colors.light.background,
          }}
        >
          <Text style={styles.listHeader}>Фильмы</Text>
          <FlatList
            horizontal
            data={trendingMovies}
            renderItem={({
              item: { title, poster_path, id, vote_average },
            }) => (
              <ItemCard
                id={id}
                key={id}
                title={title}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressMovie}
              />
            )}
          />
          <Text style={styles.listHeader}>Сериалы</Text>
          <FlatList
            horizontal
            data={trendingTvShows}
            renderItem={({ item: { name, poster_path, id, vote_average } }) => (
              <ItemCard
                id={id}
                key={id}
                title={name}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressTVShow}
              />
            )}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
