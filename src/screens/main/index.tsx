import React, { FC, useEffect } from 'react';

import { useAppDispatch } from '../../store/hooks';
import { getConfiguration } from '../../store/configuration';
import TabsNavigationContainer from '../../navigation/TabsNavigationContainer';

const MainScreen: FC = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getConfiguration());
  }, []);

  return <TabsNavigationContainer />;
};

export default MainScreen;
