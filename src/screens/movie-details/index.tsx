import React, { FC, useEffect, useMemo } from 'react';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  View,
  Text,
  Image,
  StyleSheet,
  useWindowDimensions,
  useColorScheme,
  ScrollView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { IMainNavigationScreenProps } from '../../navigation/types';
import { MainScreens } from '../../constants/screens';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import {
  clearMovieDetails,
  getMovieDetails,
  selectMovieDetails,
} from '../../store/movies';
import { selectSecureBaseUrl } from '../../store/configuration';
import CircularProgress from '../../components/shared/circular-progress';

const calculateStyles = (width: number, isDark: boolean) =>
  StyleSheet.create({
    container: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
      padding: 20,
    },
    posterContainer: {
      height: width / 2.222222,
      width: '100%',
      padding: 20,
    },
    poster: {
      width: 100,
      height: 150,
      borderRadius: 8,
      resizeMode: 'contain',
    },
    backdrop: {
      position: 'absolute',
      resizeMode: 'contain',
      height: width / 2.222222,
      width: '100%',
      top: 0,
      right: 0,
      left: (width / 2.222222 - 20) / 4,
      bottom: 0,
    },
    title: {
      textAlign: 'center',
      fontSize: 24,
      fontWeight: 'bold',
      color: isDark ? '#ffffff' : '#000000',
    },
    subTitle: {
      fontSize: 16,
      fontWeight: 'bold',
      color: isDark ? '#ffffff' : '#000000',
    },
    mainText: {
      fontSize: 16,
      color: isDark ? '#ffffff' : '#000000',
    },
    description: {
      padding: 20,
    },
  });

const MovieDetails: FC<
  NativeStackScreenProps<
    IMainNavigationScreenProps,
    MainScreens.MovieDetails,
    'Details'
  >
> = ({ route }) => {
  const {
    params: { movieId },
  } = route;
  const isDarkMode = useColorScheme() === 'dark';
  const { width } = useWindowDimensions();
  const dispatch = useAppDispatch();
  const movie = useAppSelector(selectMovieDetails);
  const secureUrl = useAppSelector(selectSecureBaseUrl);

  const styles = useMemo(
    () => calculateStyles(width, isDarkMode),
    [width, isDarkMode],
  );

  const [releaseYear, releaseDate] = useMemo(() => {
    const splittedDate = movie?.release_date.split('-');
    return [
      splittedDate?.[0] ?? '',
      [splittedDate?.[2], splittedDate?.[1], splittedDate?.[0]].join('/'),
    ];
  }, [movie]);

  const genresString = useMemo(
    () => movie?.genres.map((genre) => genre.name).join(', '),
    [movie],
  );

  const userCount = useMemo(
    () => Math.ceil((movie?.vote_average ?? 0) * 10),
    [movie],
  );

  useEffect(() => {
    if (movieId) {
      dispatch(getMovieDetails(movieId));
    }
    return () => {
      dispatch(clearMovieDetails());
    };
  }, [dispatch, movieId]);

  return movie ? (
    <ScrollView>
      <View>
        <Image
          style={styles.backdrop}
          source={{ uri: `${secureUrl}/original/${movie.backdrop_path}` }}
        />
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['rgba(31.5, 31.5, 52.5, 1)', 'rgba(31.5, 31.5, 52.5, 0)']}
          locations={[0.2, 1]}
          style={{ ...styles.backdrop, left: 0 }}
        />

        <View style={styles.posterContainer}>
          <Image
            style={styles.poster}
            source={{ uri: `${secureUrl}/original/${movie.poster_path}` }}
          />
        </View>
      </View>
      <View style={styles.container}>
        <Text style={styles.title}>{`${movie.title} (${releaseYear})`}</Text>
      </View>
      <View style={styles.container}>
        <CircularProgress value={userCount} />
        <Text style={styles.subTitle}>Пользовательский счет</Text>
      </View>
      <View style={styles.container}>
        <Text style={styles.mainText}>
          {releaseDate} • {movie.runtime}мин
        </Text>
        <Text style={styles.mainText}>{genresString}</Text>
      </View>
      <View style={styles.description}>
        {movie.tagline ? (
          <Text
            style={[
              styles.mainText,
              { textAlign: 'left', fontStyle: 'italic' },
            ]}
          >
            {movie.tagline}
          </Text>
        ) : null}
        <Text style={[styles.title, { textAlign: 'left' }]}>Обзор</Text>
        <Text
          style={[styles.mainText, { textAlign: 'left', marginBottom: 30 }]}
        >
          {movie.overview}
        </Text>
      </View>
    </ScrollView>
  ) : (
    <View>
      <Text>Ой, что-то пошло не так ((</Text>
    </View>
  );
};

export default MovieDetails;
