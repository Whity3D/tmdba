import {
  FlatList,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import React, { FC, useCallback, useEffect } from 'react';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { Colors } from 'react-native/Libraries/NewAppScreen';

import { useAppDispatch, useAppSelector } from '../../store/hooks';
import {
  getNowPlaying,
  getPopular,
  getTopRated,
  getUpcoming,
  selectNowPlayingMovies,
  selectPopularMovies,
  selectTopRatedMovies,
  selectUpcomingMovies,
} from '../../store/movies';
import AppTitle from '../../components/shared/app-title';
import colors from '../../constants/colors';
import ItemCard from '../../components/shared/item-card';
import { MainScreens, TabsScreens } from '../../constants/screens';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ITabsNavigationScreenProps } from '../../navigation/types';

const styles = StyleSheet.create({
  container: {
    padding: 5,
  },
  listHeader: {
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
});

const MoviesScreen: FC<
  NativeStackScreenProps<ITabsNavigationScreenProps, TabsScreens.Movies>
> = ({ navigation }) => {
  const isDarkMode = useColorScheme() === 'dark';
  const tabBarHeight = useBottomTabBarHeight();

  const dispatch = useAppDispatch();
  const popular = useAppSelector(selectPopularMovies);
  const topRated = useAppSelector(selectTopRatedMovies);
  const nowPlaying = useAppSelector(selectNowPlayingMovies);
  const upcoming = useAppSelector(selectUpcomingMovies);

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const handlePressItem = useCallback((id: number) => {
    navigation.navigate(MainScreens.MovieDetails, { movieId: id });
  }, []);

  useEffect(() => {
    dispatch(getPopular());
    dispatch(getTopRated());
    dispatch(getNowPlaying());
    dispatch(getUpcoming());
  }, []);

  return (
    <SafeAreaView>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        stickyHeaderIndices={[0]}
        style={backgroundStyle}
      >
        <AppTitle />
        <View
          style={{
            marginBottom: tabBarHeight,
            backgroundColor: isDarkMode
              ? colors.dark.background
              : colors.light.background,
          }}
        >
          <Text style={styles.listHeader}>Популярное</Text>
          <FlatList
            horizontal
            data={popular}
            renderItem={({
              item: { title, poster_path, id, vote_average },
            }) => (
              <ItemCard
                id={id}
                key={id}
                title={title}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressItem}
              />
            )}
          />
          <Text style={styles.listHeader}>С высокой оценкой</Text>
          <FlatList
            horizontal
            data={topRated}
            renderItem={({
              item: { title, poster_path, id, vote_average },
            }) => (
              <ItemCard
                id={id}
                key={id}
                title={title}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressItem}
              />
            )}
          />
          <Text style={styles.listHeader}>Сейчас смотрят</Text>
          <FlatList
            horizontal
            data={nowPlaying}
            renderItem={({
              item: { title, poster_path, id, vote_average },
            }) => (
              <ItemCard
                id={id}
                key={id}
                title={title}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressItem}
              />
            )}
          />
          <Text style={styles.listHeader}>Скоро</Text>
          <FlatList
            horizontal
            data={upcoming}
            renderItem={({
              item: { title, poster_path, id, vote_average },
            }) => (
              <ItemCard
                id={id}
                key={id}
                title={title}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressItem}
              />
            )}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default MoviesScreen;
