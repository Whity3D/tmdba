import React, { FC, useCallback, useEffect } from 'react';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
// import { Colors } from 'react-native/Libraries/NewAppScreen';
// import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import {
  Button,
  SafeAreaView,
  StatusBar,
  Text,
  useColorScheme,
} from 'react-native';

import { ITabsNavigationScreenProps } from '../../navigation/types';
import { MainScreens, TabsScreens } from '../../constants/screens';
import AppTitle from '../../components/shared/app-title';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import {
  getProfile,
  selectIsSigned,
  selectProfile,
  signOut,
} from '../../store/auth';

const ProfileScreen: FC<
  NativeStackScreenProps<ITabsNavigationScreenProps, TabsScreens.Profile>
> = ({ navigation }) => {
  const dispatch = useAppDispatch();
  const isDarkMode = useColorScheme() === 'dark';
  // const tabBarHeight = useBottomTabBarHeight();

  const isSigned = useAppSelector(selectIsSigned);
  const profile = useAppSelector(selectProfile);

  const handleSignIn = useCallback(() => {
    navigation.navigate(MainScreens.SignIn);
  }, []);

  const handleSignOut = useCallback(() => {
    dispatch(signOut());
  }, []);

  useEffect(() => {
    dispatch(getProfile());
  }, []);

  return (
    <SafeAreaView>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <AppTitle />
      {isSigned ? (
        <>
          <Text>Профиль</Text>
          <Text>Логин пользователя: {profile?.username}</Text>
          <Text>Имя пользователя: {profile?.name}</Text>
          <Text>Идентификатор пользователя: {profile?.id}</Text>
          <Text>Взрослый контент: {profile?.include_adult ? 'Да' : 'Нет'}</Text>
          <Button title="Выйти из аккаунта" onPress={handleSignOut} />
        </>
      ) : (
        <>
          <Text>Вы не авторизованы.</Text>
          <Text>Авторизуйтесь, чтобы увидеть избранное</Text>
          <Button title="Войти" onPress={handleSignIn} />
        </>
      )}
    </SafeAreaView>
  );
};

export default ProfileScreen;
