import React, { FC, useCallback, useEffect } from 'react';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  Button,
  SafeAreaView,
  StatusBar,
  Text,
  TextInput,
  useColorScheme,
  View,
} from 'react-native';
import { useForm, Controller } from 'react-hook-form';

import { IMainNavigationScreenProps } from '../../navigation/types';
import { MainScreens } from '../../constants/screens';
import AppTitle from '../../components/shared/app-title';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { getRequestToken, signIn, selectIsSigned } from '../../store/auth';

interface IAuthFormData {
  username: string;
  password: string;
}

const SignInScreen: FC<
  NativeStackScreenProps<IMainNavigationScreenProps, MainScreens.SignIn>
> = ({ navigation }) => {
  const dispatch = useAppDispatch();
  const isSigned = useAppSelector(selectIsSigned);
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<IAuthFormData>({ defaultValues: { username: '', password: '' } });

  const isDarkMode = useColorScheme() === 'dark';

  const handleAuth = useCallback(async (data: IAuthFormData) => {
    dispatch(signIn(data));
  }, []);

  useEffect(() => {
    if (isSigned) {
      navigation.goBack();
    }
  }, [isSigned]);

  useEffect(() => {
    dispatch(getRequestToken());
  }, []);

  return (
    <SafeAreaView>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View>
        <AppTitle />
        <Text>Авторизация</Text>
        <Controller
          name="username"
          control={control}
          rules={{ required: true }}
          render={({ field: { onBlur, onChange, value } }) => (
            <TextInput
              value={value}
              onChangeText={onChange}
              onBlur={onBlur}
              placeholder="Логин"
            />
          )}
        />
        {errors.username ? <Text>Обязательно</Text> : null}
        <Controller
          name="password"
          control={control}
          rules={{ required: true }}
          render={({ field: { onBlur, onChange, value } }) => (
            <TextInput
              value={value}
              onChangeText={onChange}
              onBlur={onBlur}
              placeholder="Пароль"
              secureTextEntry
            />
          )}
        />
        {errors.password ? <Text>Обязательно</Text> : null}
        <Button title="войти" onPress={handleSubmit(handleAuth)} />
      </View>
    </SafeAreaView>
  );
};

export default SignInScreen;
