import React, { FC, useEffect, useMemo } from 'react';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  View,
  Text,
  Image,
  StyleSheet,
  useWindowDimensions,
  useColorScheme,
  ScrollView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { IMainNavigationScreenProps } from '../../navigation/types';
import { MainScreens } from '../../constants/screens';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { selectSecureBaseUrl } from '../../store/configuration';
import CircularProgress from '../../components/shared/circular-progress';
import {
  clearTVShowDetails,
  getTVSHowDetails,
  selectTVShowDetails,
} from '../../store/tv-shows';

const calculateStyles = (width: number, isDark: boolean) =>
  StyleSheet.create({
    container: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
      padding: 20,
    },
    posterContainer: {
      height: width / 2.222222,
      width: '100%',
      padding: 20,
    },
    poster: {
      width: 100,
      height: 150,
      borderRadius: 8,
      resizeMode: 'contain',
    },
    backdrop: {
      position: 'absolute',
      resizeMode: 'contain',
      height: width / 2.222222,
      width: '100%',
      top: 0,
      right: 0,
      left: (width / 2.222222 - 20) / 4,
      bottom: 0,
    },
    title: {
      textAlign: 'center',
      fontSize: 24,
      fontWeight: 'bold',
      color: isDark ? '#ffffff' : '#000000',
    },
    subTitle: {
      fontSize: 16,
      fontWeight: 'bold',
      color: isDark ? '#ffffff' : '#000000',
    },
    mainText: {
      fontSize: 16,
      color: isDark ? '#ffffff' : '#000000',
    },
    description: {
      padding: 20,
    },
  });

const TVShowDetails: FC<
  NativeStackScreenProps<
    IMainNavigationScreenProps,
    MainScreens.TVShowDetails,
    'Details'
  >
> = ({ route }) => {
  const {
    params: { showId },
  } = route;
  const isDarkMode = useColorScheme() === 'dark';
  const { width } = useWindowDimensions();
  const dispatch = useAppDispatch();
  const tvShow = useAppSelector(selectTVShowDetails);
  const secureUrl = useAppSelector(selectSecureBaseUrl);

  const styles = useMemo(
    () => calculateStyles(width, isDarkMode),
    [width, isDarkMode],
  );

  const [releaseYear, releaseDate] = useMemo(() => {
    const splittedDate = tvShow?.first_air_date.split('-');
    return [
      splittedDate?.[0] ?? '',
      [splittedDate?.[2], splittedDate?.[1], splittedDate?.[0]].join('/'),
    ];
  }, [tvShow]);

  const genresString = useMemo(
    () => tvShow?.genres.map((genre) => genre.name).join(', '),
    [tvShow],
  );

  const runtimeString = useMemo(() => {
    const episodes = tvShow?.episode_run_time ?? [];
    if (episodes.length > 1) {
      return `${episodes[0]} - ${episodes[episodes.length - 1]}`;
    }
    return episodes[0];
  }, [tvShow]);

  const userCount = useMemo(
    () => Math.ceil((tvShow?.vote_average ?? 0) * 10),
    [tvShow],
  );

  useEffect(() => {
    if (showId) {
      dispatch(getTVSHowDetails(showId));
    }
    return () => {
      dispatch(clearTVShowDetails());
    };
  }, [dispatch, showId]);

  return tvShow ? (
    <ScrollView>
      <View>
        <Image
          style={styles.backdrop}
          source={{ uri: `${secureUrl}/original/${tvShow.backdrop_path}` }}
        />
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['rgba(31.5, 31.5, 52.5, 1)', 'rgba(31.5, 31.5, 52.5, 0)']}
          locations={[0.2, 1]}
          style={{ ...styles.backdrop, left: 0 }}
        />

        <View style={styles.posterContainer}>
          <Image
            style={styles.poster}
            source={{ uri: `${secureUrl}/original/${tvShow.poster_path}` }}
          />
        </View>
      </View>
      <View style={styles.container}>
        <Text style={styles.title}>{`${tvShow.name} (${releaseYear})`}</Text>
      </View>
      <View style={styles.container}>
        <CircularProgress value={userCount} />
        <Text style={styles.subTitle}>Пользовательский счет</Text>
      </View>
      <View style={styles.container}>
        <Text style={styles.mainText}>
          {releaseDate} • {runtimeString} мин
        </Text>
        <Text style={styles.mainText}>{genresString}</Text>
      </View>
      <View style={styles.description}>
        {tvShow.tagline ? (
          <Text
            style={[
              styles.mainText,
              { textAlign: 'left', fontStyle: 'italic' },
            ]}
          >
            {tvShow.tagline}
          </Text>
        ) : null}
        <Text style={[styles.title, { textAlign: 'left' }]}>Обзор</Text>
        <Text
          style={[styles.mainText, { textAlign: 'left', marginBottom: 30 }]}
        >
          {tvShow.overview}
        </Text>
      </View>
    </ScrollView>
  ) : (
    <View>
      <Text>Ой, что-то пошло не так ((</Text>
    </View>
  );
};

export default TVShowDetails;
