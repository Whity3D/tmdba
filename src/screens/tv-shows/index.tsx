import {
  FlatList,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import React, { FC, useCallback, useEffect } from 'react';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { Colors } from 'react-native/Libraries/NewAppScreen';

import { useAppDispatch, useAppSelector } from '../../store/hooks';
import AppTitle from '../../components/shared/app-title';
import colors from '../../constants/colors';
import ItemCard from '../../components/shared/item-card';
import {
  getAiringToday,
  getOnAir,
  getPopular,
  getTopRated,
  selectAiringTodayTVShows,
  selectOnAirTVShows,
  selectPopularTVShows,
  selectTopRatedTVShows,
} from '../../store/tv-shows';
import { MainScreens, TabsScreens } from '../../constants/screens';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ITabsNavigationScreenProps } from '../../navigation/types';

const styles = StyleSheet.create({
  container: {
    padding: 5,
  },
  listHeader: {
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
});

const TVShowsScreen: FC<
  NativeStackScreenProps<ITabsNavigationScreenProps, TabsScreens.TV>
> = ({ navigation }) => {
  const isDarkMode = useColorScheme() === 'dark';
  const tabBarHeight = useBottomTabBarHeight();

  const dispatch = useAppDispatch();
  const popular = useAppSelector(selectPopularTVShows);
  const topRated = useAppSelector(selectTopRatedTVShows);
  const onAir = useAppSelector(selectOnAirTVShows);
  const airingToday = useAppSelector(selectAiringTodayTVShows);

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const handlePressItem = useCallback((id: number) => {
    navigation.navigate(MainScreens.TVShowDetails, { showId: id });
  }, []);

  useEffect(() => {
    dispatch(getPopular());
    dispatch(getTopRated());
    dispatch(getAiringToday());
    dispatch(getOnAir());
  }, []);

  return (
    <SafeAreaView>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        stickyHeaderIndices={[0]}
        style={backgroundStyle}
      >
        <AppTitle />
        <View
          style={{
            marginBottom: tabBarHeight,
            backgroundColor: isDarkMode
              ? colors.dark.background
              : colors.light.background,
          }}
        >
          <Text style={styles.listHeader}>Популярное</Text>
          <FlatList
            horizontal
            data={popular}
            renderItem={({ item: { name, poster_path, id, vote_average } }) => (
              <ItemCard
                id={id}
                key={id}
                title={name}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressItem}
              />
            )}
          />
          <Text style={styles.listHeader}>С высокой оценкой</Text>
          <FlatList
            horizontal
            data={topRated}
            renderItem={({ item: { name, poster_path, id, vote_average } }) => (
              <ItemCard
                id={id}
                key={id}
                title={name}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressItem}
              />
            )}
          />
          <Text style={styles.listHeader}>Сейчас смотрят</Text>
          <FlatList
            horizontal
            data={onAir}
            renderItem={({ item: { name, poster_path, id, vote_average } }) => (
              <ItemCard
                id={id}
                key={id}
                title={name}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressItem}
              />
            )}
          />
          <Text style={styles.listHeader}>Сегодня</Text>
          <FlatList
            horizontal
            data={airingToday}
            renderItem={({ item: { name, poster_path, id, vote_average } }) => (
              <ItemCard
                id={id}
                key={id}
                title={name}
                posterPath={poster_path}
                rating={vote_average}
                onPress={handlePressItem}
              />
            )}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default TVShowsScreen;
