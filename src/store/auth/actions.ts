import { createAsyncThunk } from '@reduxjs/toolkit';
import EncryptedStorage from 'react-native-encrypted-storage';
import axios from 'axios';

import { createAsyncThunkSequence } from '../../utils/store/async-thunk-sequence';

import { API_V3_KEY, tmdb_api } from '../constants';

import {
  IGetProfileResponse,
  ICreateSessionResponse,
  IGetRequestTokenResponse,
  ILoginWithPasswordArguments,
  ILoginWithPasswordResponse,
} from './types';
import { areaName } from './constants';

export const getRequestToken = createAsyncThunk(
  `${areaName}/getToken`,
  async () => {
    const response = await axios.get<IGetRequestTokenResponse>(
      `${tmdb_api}/authentication/token/new`,
      {
        params: {
          api_key: API_V3_KEY,
        },
      },
    );

    await EncryptedStorage.setItem(
      'requestToken',
      JSON.stringify(response.data),
    );
  },
);

export const validateRequestToken = createAsyncThunk<
  void,
  ILoginWithPasswordArguments
>(`${areaName}/validateToken`, async (payload) => {
  const requestToken = await EncryptedStorage.getItem('requestToken');
  if (requestToken != null) {
    const response = await axios.post<ILoginWithPasswordResponse>(
      `${tmdb_api}/authentication/token/validate_with_login`,
      {
        ...payload,
        request_token: JSON.parse(requestToken).request_token,
      },
      {
        params: {
          api_key: API_V3_KEY,
        },
      },
    );

    await EncryptedStorage.setItem(
      'requestToken',
      JSON.stringify(response.data),
    );
  }
});

export const createSession = createAsyncThunk<boolean>(
  `${areaName}/createSession`,
  async () => {
    const requestToken = await EncryptedStorage.getItem('requestToken');
    if (requestToken != null) {
      const response = await axios.post<ICreateSessionResponse>(
        `${tmdb_api}/authentication/session/new`,
        { request_token: JSON.parse(requestToken).request_token },
        {
          params: {
            api_key: API_V3_KEY,
          },
        },
      );

      await EncryptedStorage.setItem(
        'sessionId',
        JSON.stringify(response.data),
      );
      return true;
    }
    return false;
  },
);

export const getProfile = createAsyncThunk<IGetProfileResponse | undefined>(
  `${areaName}/getProfile`,
  async () => {
    const sessionId = await EncryptedStorage.getItem('sessionId');
    if (sessionId != null) {
      const response = await axios.get<IGetProfileResponse>(
        `${tmdb_api}/account`,
        {
          params: {
            api_key: API_V3_KEY,
            session_id: JSON.parse(sessionId).session_id,
          },
        },
      );
      return response.data;
    }
    return;
  },
);

export const signIn = createAsyncThunkSequence<ILoginWithPasswordArguments>(
  `${areaName}/signIn`,
  [(data) => validateRequestToken(data), createSession],
);

export const signOut = createAsyncThunk(`${areaName}/signOut`, async () => {
  await EncryptedStorage.removeItem('sessionId');
  await EncryptedStorage.removeItem('requestToken');
});
