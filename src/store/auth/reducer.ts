import { createSlice } from '@reduxjs/toolkit';

import { areaName } from './constants';
import { createSession, getProfile, signOut } from './actions';
import { IAuthState } from './types';

const authInitialState: IAuthState = {
  isSigned: false,
};

const authSlice = createSlice({
  name: areaName,
  initialState: authInitialState,
  reducers: {},
  extraReducers: ({ addCase }) => {
    addCase(createSession.fulfilled, (draft, { payload }) => {
      draft.isSigned = payload;
    });
    addCase(signOut.fulfilled, (draft) => {
      draft.isSigned = false;
      draft.profile = undefined;
    });
    addCase(getProfile.fulfilled, (draft, { payload }) => {
      if (payload) {
        draft.profile = payload;
      }
    });
  },
});

const { reducer } = authSlice;

export default reducer;
