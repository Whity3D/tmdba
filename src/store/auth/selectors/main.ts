import { createSelector } from '@reduxjs/toolkit';

import { IRootState } from '../../types';

const selectAuthRootState = (state: IRootState) => state.auth;

export const selectIsSigned = createSelector(
  selectAuthRootState,
  ({ isSigned }) => isSigned,
);

export const selectProfile = createSelector(
  selectAuthRootState,
  ({ profile }) => profile,
);
