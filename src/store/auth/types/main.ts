import { IGetProfileResponse } from './actions';

export interface IAuthState {
  isSigned: boolean;
  profile?: IGetProfileResponse;
}
