import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

import { API_V3_KEY, tmdb_api } from '../constants';
import type { IAsyncThunkConfig } from '../types';

import type { IConfiguration } from './types';
import { configurationDomain } from './constants';

export const getConfiguration = createAsyncThunk<
  IConfiguration,
  undefined,
  IAsyncThunkConfig
>(`${configurationDomain}/get`, async () => {
  const response = await axios.get(`${tmdb_api}/configuration`, {
    params: {
      api_key: API_V3_KEY,
    },
  });
  return response.data;
});
