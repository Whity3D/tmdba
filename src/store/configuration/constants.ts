import type { IConfigurationState } from './types';

export const configurationDomain = 'configuration';

export const configurationInitialState: IConfigurationState = {};
