import { createSlice } from '@reduxjs/toolkit';
import { configurationDomain, configurationInitialState } from './constants';
import { getConfiguration } from './actions';

const configurationSlice = createSlice({
  name: configurationDomain,
  initialState: configurationInitialState,
  reducers: {},
  extraReducers: ({ addCase }) => {
    addCase(getConfiguration.fulfilled, (_, { payload }) => {
      console.log(payload);
      return { ...payload };
    });
  },
});

const { reducer } = configurationSlice;
export default reducer;
