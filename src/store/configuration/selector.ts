import { IRootState } from '../types';
import { createSelector } from '@reduxjs/toolkit';

const rootSelector = (state: IRootState) => state.configuration;

export const selectImagesConfiguration = createSelector(
  rootSelector,
  ({ images }) => images,
);

export const selectBaseUrl = createSelector(
  selectImagesConfiguration,
  (images) => images?.base_url,
);

export const selectSecureBaseUrl = createSelector(
  selectImagesConfiguration,
  (images) => images?.secure_base_url,
);

export const selectPosterSizes = createSelector(
  selectImagesConfiguration,
  (images) => images?.poster_sizes,
);
