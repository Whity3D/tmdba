export const tmdb_api = 'https://api.themoviedb.org/3';

export const API_V3_KEY = 'cd70bd3d740737d4c010239a62ab9297';

export const API_V4_KEY =
  'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjZDcwYmQzZDc0MDczN2Q0YzAxMDIzOWE2MmFiOTI5NyIsInN1YiI6IjViNjJiYmY1YzNhMzY4MTg5NzAyOTY3YiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.H5WARhEynFwVORqQmUqOYdocXLK-dyOiyzrqiy3yzzk';

export enum MovieStatus {
  Rumored = 'Rumored',
  Planned = 'Planned',
  InProduction = 'In Production',
  PostProduction = 'Post Production',
  Released = 'Released',
  Canceled = 'Canceled',
}
