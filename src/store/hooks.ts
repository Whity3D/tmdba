import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import type { IAppDispatch, IRootState } from './types';

export const useAppDispatch: () => IAppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<IRootState> = useSelector;
