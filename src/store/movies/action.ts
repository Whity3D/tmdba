import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

import { API_V3_KEY, tmdb_api } from '../constants';
import type { IAsyncThunkConfig, IMovieDetails } from '../types';

import { moviesDomain } from './constants';
import type { IGetMoviesResponse } from './types';

export const getPopular = createAsyncThunk<
  IGetMoviesResponse,
  undefined,
  IAsyncThunkConfig
>(`${moviesDomain}/popular`, async () => {
  const response = await axios.get(`${tmdb_api}/movie/popular`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });
  return response.data;
});

export const getTrending = createAsyncThunk<
  IGetMoviesResponse,
  'day' | 'week',
  IAsyncThunkConfig
>(`${moviesDomain}/trending`, async (peroid) => {
  const response = await axios.get(`${tmdb_api}/trending/movie/${peroid}`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });

  return response.data;
});

export const getTopRated = createAsyncThunk<
  IGetMoviesResponse,
  undefined,
  IAsyncThunkConfig
>(`${moviesDomain}/topRated`, async () => {
  const response = await axios.get(`${tmdb_api}/movie/top_rated`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });

  return response.data;
});

export const getNowPlaying = createAsyncThunk<
  IGetMoviesResponse,
  undefined,
  IAsyncThunkConfig
>(`${moviesDomain}/nowPlaying`, async () => {
  const response = await axios.get(`${tmdb_api}/movie/now_playing`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });

  return response.data;
});

export const getUpcoming = createAsyncThunk<
  IGetMoviesResponse,
  undefined,
  IAsyncThunkConfig
>(`${moviesDomain}/upcoming`, async () => {
  const response = await axios.get(`${tmdb_api}/movie/upcoming`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });

  return response.data;
});

export const getMovieDetails = createAsyncThunk<IMovieDetails, number>(
  `${moviesDomain}/getDetails`,
  async (movieId) => {
    const response = await axios.get(`${tmdb_api}/movie/${movieId}`, {
      params: {
        api_key: API_V3_KEY,
        language: 'ru-RU',
      },
    });

    return response.data;
  },
);
