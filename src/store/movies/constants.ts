import type { IMovieState } from './types';

export const moviesDomain = 'movies';

export const moviesInitialState: IMovieState = {
  popular: {
    data: [],
    pagination: {
      page: 1,
      totalCounts: 0,
      totalPages: 0,
    },
  },
  trending: {
    data: [],
    pagination: {
      page: 1,
      totalCounts: 0,
      totalPages: 0,
    },
  },
  nowPlaying: {
    data: [],
    pagination: { page: 1, totalCounts: 0, totalPages: 0 },
  },
  topRated: {
    data: [],
    pagination: { page: 1, totalCounts: 0, totalPages: 0 },
  },
  upcoming: {
    data: [],
    pagination: { page: 1, totalCounts: 0, totalPages: 0 },
  },
};
