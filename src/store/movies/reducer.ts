import { createSlice } from '@reduxjs/toolkit';

import { moviesDomain, moviesInitialState } from './constants';
import {
  getMovieDetails,
  getNowPlaying,
  getPopular,
  getTopRated,
  getTrending,
  getUpcoming,
} from './action';

const moviesSlice = createSlice({
  name: moviesDomain,
  initialState: moviesInitialState,
  reducers: {
    clearMovieDetails: (draft) => {
      draft.movieDetails = undefined;
    },
  },
  extraReducers: ({ addCase }) => {
    addCase(getPopular.fulfilled, (draft, { payload }) => {
      draft.popular = payload.results;
    });
    addCase(getTopRated.fulfilled, (draft, { payload }) => {
      draft.topRated = payload.results;
    });
    addCase(getNowPlaying.fulfilled, (draft, { payload }) => {
      draft.nowPlaying = payload.results;
    });
    addCase(getTrending.fulfilled, (draft, { payload }) => {
      draft.trending = payload.results;
    });
    addCase(getUpcoming.fulfilled, (draft, { payload }) => {
      draft.upcoming = payload.results;
    });
    addCase(getMovieDetails.fulfilled, (draft, { payload }) => {
      draft.movieDetails = payload;
    });
  },
});

const { reducer, actions } = moviesSlice;

export const { clearMovieDetails } = actions;

export default reducer;
