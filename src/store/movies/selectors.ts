import type { IRootState } from '../types';
import { createSelector } from '@reduxjs/toolkit';

const rootSelector = (state: IRootState) => state.movies;

export const selectPopularMovies = createSelector(
  rootSelector,
  ({ popular }) => popular,
);

export const selectTopRatedMovies = createSelector(
  rootSelector,
  ({ topRated }) => topRated,
);

export const selectUpcomingMovies = createSelector(
  rootSelector,
  ({ upcoming }) => upcoming,
);

export const selectNowPlayingMovies = createSelector(
  rootSelector,
  ({ nowPlaying }) => nowPlaying,
);

export const selectTrendingMovies = createSelector(
  rootSelector,
  ({ trending }) => trending,
);

export const selectMovieDetails = createSelector(
  rootSelector,
  ({ movieDetails }) => movieDetails,
);
