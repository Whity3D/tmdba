import { IMovie } from '../../types';

export interface IGetMoviesResponse {
  page: number;
  total_results: number;
  total_pages: number;
  results: IMovie[];
}
