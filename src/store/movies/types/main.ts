import { IMovie, IMovieDetails } from '../../types';

export interface IMovieState {
  popular: IMovie[];
  topRated: IMovie[];
  nowPlaying: IMovie[];
  upcoming: IMovie[];
  trending: IMovie[];
  movieDetails?: IMovieDetails;
}
