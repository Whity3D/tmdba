import { combineReducers } from '@reduxjs/toolkit';

import authReducer from './auth/reducer';
import moviesReducer from './movies/reducer';
import configReducer from './configuration/reducer';
import tvShowReducer from './tv-shows/reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  configuration: configReducer,
  movies: moviesReducer,
  tvShows: tvShowReducer,
});

export default rootReducer;
