import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

import { IAsyncThunkConfig, ITVShowDetails } from '../types';
import { API_V3_KEY, tmdb_api } from '../constants';

import { tvShowsDomain } from './constants';
import { ITVShowsResponse } from './types';

export const getTrending = createAsyncThunk<
  ITVShowsResponse,
  'day' | 'week',
  IAsyncThunkConfig
>(`${tvShowsDomain}/trending`, async (peroid) => {
  const response = await axios.get(`${tmdb_api}/trending/tv/${peroid}`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });

  return response.data;
});

export const getPopular = createAsyncThunk<
  ITVShowsResponse,
  undefined,
  IAsyncThunkConfig
>(`${tvShowsDomain}/popular`, async () => {
  const response = await axios.get(`${tmdb_api}/tv/popular`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });
  return response.data;
});

export const getTopRated = createAsyncThunk<
  ITVShowsResponse,
  undefined,
  IAsyncThunkConfig
>(`${tvShowsDomain}/topRated`, async () => {
  const response = await axios.get(`${tmdb_api}/tv/top_rated`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });
  return response.data;
});

export const getAiringToday = createAsyncThunk<
  ITVShowsResponse,
  undefined,
  IAsyncThunkConfig
>(`${tvShowsDomain}/airingToday`, async () => {
  const response = await axios.get(`${tmdb_api}/tv/airing_today`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });
  return response.data;
});

export const getOnAir = createAsyncThunk<
  ITVShowsResponse,
  undefined,
  IAsyncThunkConfig
>(`${tvShowsDomain}/onAir`, async () => {
  const response = await axios.get(`${tmdb_api}/tv/on_the_air`, {
    params: {
      api_key: API_V3_KEY,
      language: 'ru-RU',
      page: 1,
      region: 'RU',
    },
  });
  return response.data;
});

export const getTVSHowDetails = createAsyncThunk<ITVShowDetails, number>(
  `${tvShowsDomain}/details`,
  async (id) => {
    const response = await axios.get(`${tmdb_api}/tv/${id}`, {
      params: {
        api_key: API_V3_KEY,
        language: 'ru-RU',
      },
    });

    return response.data;
  },
);
