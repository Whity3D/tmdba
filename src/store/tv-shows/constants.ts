import { ITVShowsState } from './types';

export const tvShowsDomain = 'tvShows';

export const tvShowsInitialState: ITVShowsState = {
  popular: [],
  trending: [],
  airingToday: [],
  onAir: [],
  topRated: [],
};
