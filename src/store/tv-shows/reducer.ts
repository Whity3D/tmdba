import { createSlice } from '@reduxjs/toolkit';

import { tvShowsDomain, tvShowsInitialState } from './constants';
import {
  getAiringToday,
  getOnAir,
  getPopular,
  getTopRated,
  getTrending,
  getTVSHowDetails,
} from './action';

const tvShowSlice = createSlice({
  name: tvShowsDomain,
  initialState: tvShowsInitialState,
  reducers: {
    clearTVShowDetails: (draft) => {
      draft.details = undefined;
    },
  },
  extraReducers: ({ addCase }) => {
    addCase(getTrending.fulfilled, (draft, { payload }) => {
      draft.trending = payload.results;
    });
    addCase(getPopular.fulfilled, (draft, { payload }) => {
      draft.popular = payload.results;
    });
    addCase(getTopRated.fulfilled, (draft, { payload }) => {
      draft.topRated = payload.results;
    });
    addCase(getAiringToday.fulfilled, (draft, { payload }) => {
      draft.airingToday = payload.results;
    });
    addCase(getOnAir.fulfilled, (draft, { payload }) => {
      draft.onAir = payload.results;
    });
    addCase(getTVSHowDetails.fulfilled, (draft, { payload }) => {
      draft.details = payload;
    });
  },
});

const { reducer, actions } = tvShowSlice;

export const { clearTVShowDetails } = actions;

export default reducer;
