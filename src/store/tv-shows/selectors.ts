import { IRootState } from '../types';
import { createSelector } from '@reduxjs/toolkit';

const rootSelector = (state: IRootState) => state.tvShows;

export const selectTrendingTVShows = createSelector(
  rootSelector,
  ({ trending }) => trending,
);
export const selectPopularTVShows = createSelector(
  rootSelector,
  ({ popular }) => popular,
);
export const selectTopRatedTVShows = createSelector(
  rootSelector,
  ({ topRated }) => topRated,
);
export const selectAiringTodayTVShows = createSelector(
  rootSelector,
  ({ airingToday }) => airingToday,
);
export const selectOnAirTVShows = createSelector(
  rootSelector,
  ({ onAir }) => onAir,
);

export const selectTVShowDetails = createSelector(
  rootSelector,
  ({ details }) => details,
);
