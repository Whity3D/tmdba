import { ITVShow } from '../../types';

export interface ITVShowsResponse {
  page: number;
  total_results: number;
  total_pages: number;
  results: ITVShow[];
}
