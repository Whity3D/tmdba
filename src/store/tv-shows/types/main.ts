import { ITVShow, ITVShowDetails } from '../../types';

export interface ITVShowsState {
  popular: ITVShow[];
  trending: ITVShow[];
  topRated: ITVShow[];
  airingToday: ITVShow[];
  onAir: ITVShow[];
  details?: ITVShowDetails;
}
