import { store } from './config';
import { MovieStatus } from './constants';

export type IStore = typeof store;

export type IRootState = ReturnType<IStore['getState']>;

export type IAppDispatch = IStore['dispatch'];

export interface IAsyncThunkConfig {
  state: IRootState;
  dispatch: IAppDispatch;
}

export interface IProductionCompany {
  name: string;
  id: number;
  logo_path: string | null;
  origin_country: string;
}

export interface IProductionCountry {
  iso_3166_1: string;
  name: string;
}

export interface ISpokenLanguage {
  iso_639_1: string;
  name: string;
}

export interface IGenre {
  id: number;
  name: string;
}

export interface IMovie {
  poster_path: string | null;
  adult: boolean;
  overview: string;
  release_date: string;
  genre_ids: number[];
  id: number;
  original_title: string;
  original_language: string;
  title: string;
  backdrop_path: string | null;
  popularity: number;
  vote_count: number;
  video: boolean;
  vote_average: number;
}

export interface ICreator {
  id: number;
  credit_id: string;
  name: string;
  gender: number;
  profile_path: string | null;
}

export interface INetwork {
  name: string;
  id: number;
  logo_Path: string | null;
  origin_country: string;
}

export interface ILastEpisode {
  air_date: string;
  episode_number: number;
  id: number;
  name: string;
  overview: string;
  production_code: string;
  season_number: number;
  still_path: string | null;
  vote_average: number;
  vote_count: number;
}

export interface ISeason {
  air_date: string;
  episod_count: number;
  id: number;
  name: string;
  overview: string;
  poster_path: string | null;
  season_number: number;
}

export interface IMovieDetails extends Omit<IMovie, 'genre_ids'> {
  belongs_to_collection: {} | null;
  budget: number;
  genres: IGenre[];
  homepage: string | null;
  imdb_id: string | null;
  production_companies: IProductionCompany[];
  production_countries: IProductionCountry[];
  revenue: number;
  runtime: number | null;
  spoken_languages: ISpokenLanguage[];
  status: MovieStatus;
  tagline: string | null;
}

export interface ITVShow {
  poster_path: string | null;
  popularity: number;
  id: number;
  backdrop_path: string | null;
  vote_average: number;
  overview: string;
  first_air_date: string;
  origin_country: string[];
  genre_ids: number[];
  original_language: string;
  vote_count: number;
  name: string;
  original_name: string;
}

export interface ITVShowDetails extends Omit<ITVShow, 'genre_ids'> {
  created_by: ICreator[];
  episode_run_time: number[];
  genres: IGenre[];
  homepage: string;
  in_production: boolean;
  languages: string[];
  last_air_date: string;
  last_episode_to_air: ILastEpisode;
  next_episode_to_air: number;
  networks: INetwork[];
  number_of_episodes: number;
  number_of_seasons: number;
  overview: string;
  popularity: number;
  production_companies: IProductionCompany[];
  production_countries: IProductionCountry[];
  seasons: ISeason[];
  spoken_languages: ISpokenLanguage[];
  status: string;
  tagline: string;
  type: string;
}
