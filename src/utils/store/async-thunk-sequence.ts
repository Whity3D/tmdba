import { AsyncThunk, createAsyncThunk } from '@reduxjs/toolkit';
import pAll from 'p-all';

import { IAsyncThunkConfig } from '../../store/types';

export const createAsyncThunkSequence = <TA = unknown>(
  typePrefix: string,
  list: ((data: TA) => void)[] | unknown[],
): AsyncThunk<unknown, TA, IAsyncThunkConfig> =>
  createAsyncThunk<unknown, TA, IAsyncThunkConfig>(
    typePrefix,
    (arguments_, { dispatch, rejectWithValue }) =>
      pAll(
        (typeof list === 'function'
          ? // @ts-expect-error: TODO: разобраться с типами
            list(arguments_)
          : list
        ).map(
          (
              // @ts-expect-error: TODO: разобраться с типами
              function_,
            ) =>
            () =>
              Promise.resolve(dispatch(function_(arguments_))).then((action) =>
                action.type.endsWith('/rejected')
                  ? Promise.reject(action)
                  : action,
              ),
        ),
        { concurrency: 1 },
      ).catch((error: unknown) => {
        if (error instanceof Error) {
          // eslint-disable-next-line no-console -- Логируем ошибку в коде
          console.error(error);
        }

        return rejectWithValue(String(error));
      }),
  );
